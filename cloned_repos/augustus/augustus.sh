#!/bin/bash
#sort bam file with samtools, this step sorts on the basis of reference ID
samtools sort -@ 4 -m 24G yama_plus.bam yama_plus_s

#sort bam file with samtools, this step sorts on the basis of query ID
samtools sort -n -@ 4 -m 24G yama_plus_s.bam yama_plus_ss

#filter the query ID sorted bam file for ambuguous reads (mapped at multiple locations)
/home/antariksh/software/augustus-3.2.3/bin/filterBam --uniq --in yama_plus_ss.bam --out yama_plus_ss_filter.bam

#Again sort the filtered bam file on the basis of ref ID
samtools sort -@ 4 -m 24G yama_plus_ss_filter.bam yama_plus_s_filter

#generate intron hints with augustus bam2hints (it uses query ID sorted bam)
/home/antariksh/software/augustus-3.2.3/bin/bam2hints --intronsonly --in=yama_plus_ss_filter.bam --out=yama_plus_intronhints.gff

#generate wiggle file with bam2wig (it uses ref ID sorted bam)
/home/antariksh/software/augustus-3.2.3/bin/bam2wig -t yama_plus yama_plus_s_filter.bam >> yama_plus.wig

#generate exon hints from wig file
/home/antariksh/software/augustus-3.2.3/scripts/wig2hints.pl --type=exonpart <yama_plus.wig > yama_plus_ephints.gff

#concatenate the exon and intrin hints file
cat yama_plus_ephints.gff yama_plus_intronhints.gff > yama_plus_all_hints.gff

#replace src=X with src=M in the final hints file
sed -i 's/src=X/src=M/g' yama_plus_all_hints.gff

#set the augustus config file parameters
#run augustus
#!/bin/bash
#$ -N aug_YP
#$ -P KSU-GEN-BIOINFO
#$ -l h_rt=72:00:00
#$ -l mem=2G
#$ -pe single 4
#$ -j y

/homes/antariksh/software/augustus/bin/augustus --species=chlamydomonas --extrinsicCfgFile=/homes/antariksh/software/augustus/config/extrinsic/extrinsic.cfg --alternatives-from-evidence=true --hintsfile=/homes/antariksh/augustus/yama_plus_all_hints.gff --allow_hinted_splicesites=atac --introns=on --genemodel=complete /homes/antariksh/augustus/yama_plus.fa > /homes/antariksh/augustus/yama_plus_aug.out
