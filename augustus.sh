#!/bin/bash
#$ -N aug_YP
#$ -P KSU-GEN-BIOINFO
#$ -l h_rt=24:00:00
#$ -l mem=4G
#$ -pe single 4
#$ -j y

/homes/antariksh/software/augustus/bin/augustus --species=chlamydomonas --extrinsicCfgFile=/homes/antariksh/software/augustus/config/extrinsic/extrinsic.cfg --alternatives-from-evidence=true --hintsfile=/homes/antariksh/augustus/yama_plus_all_hints.gff --allow_hinted_splicesites=atac --genemodel=complete --introns=on --codingseq=on --gff3=on /homes/antariksh/augustus/yama_plus.fa > /homes/antariksh/augustus/yama_plus_aug.out
